import React from 'react';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from './component/card/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import homeImg from '../src/assets/hotel.jpeg';

function App() {

  const navOnCard = () => {
    window.location.href = "https://unsplash.com/"
  }

  const CardWithoutImage = () => {
    return (
      <div>
        <p>
          HIREABLE ROOMS
        </p>
        <h3>
          Anything room at Green Square Libray
        </h3>
        <p className="tl-card-footer">
          Zetland
        </p>
      </div>
    )
  }

  const CardWithImage = () => {
    return (
      <div>
        <header>
          <figure>
            <img src={homeImg} alt="Logo" width={'100%'} height={'200px'} />
          </figure>
        </header>
        <p>
          HIREABLE ROOMS
        </p>
        <h3>
          Anything room at Green Square Libray
        </h3>
        <p className="tl-card-footer">
          Zetland
        </p>
      </div>
    )
  }

  return (
    <div className="app-container">
      <header className="main-header">
        <h2>Responsive Cards</h2>
      </header>
      <hr />
      <Container fluid="lg" className="main-container">
        <header>
          <h4>Card without image</h4>
        </header>

        <Row>
          {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15].map((val, key) => (
            <Col xs={12} md={6} lg={4} className="card-col" key={key}>
              <Card onClick={navOnCard}>
                <CardWithoutImage />
              </Card>
            </Col>
          ))
          }
        </Row>
        <header className="sub-header">
          <h4>Card with images</h4>
        </header>
        <Row>
          {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15].map((val, key) => (
            <Col xs={12} md={6} lg={4} className="card-col" key={key}>
              <Card onClick={navOnCard}>
                <CardWithImage />
              </Card>
            </Col>
          ))
          }
        </Row>
      </Container>
    </div >
  );
}

export default App;
