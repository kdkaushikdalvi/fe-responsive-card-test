import { render, cleanup } from '@testing-library/react';
import Card from './component/card/Card';
afterEach(cleanup);

test('card verify children render', () => {
  const { getByTestId } = render(
    <Card>
      <div data-testid="card-id">
        card inner data
      </div>
    </Card>
  );

  const cardId = getByTestId('card-id');
  expect(getByTestId('card-id')).toBeInTheDocument(cardId)
});
