import React from 'react';
import './Card.scss';

function Card(props) {
    return (
        <div className={`tl-main-container tl-animate`} onClick={props.onClick}>
            {props.children}
        </div>
    );
}

export default Card;